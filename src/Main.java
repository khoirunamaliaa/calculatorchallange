//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public int addNumber(int a, int b){
        return a + b;
    }

    int result = addNumber(5, 10);
    public static void main(String[] args) {
        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
        // to see how IntelliJ IDEA suggests fixing it.
        int num1 = 10;
        int num2 = 20;
        int sum = num1 + num2;

        int[] numbers = {1, 2, 3, 4, 5};

        System.out.println("Sum: " + sum);
        System.out.println(numbers);
    }
}
